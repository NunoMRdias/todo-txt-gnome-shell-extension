// This extension was developed by Bart Libert
//
// Based on code by :
// * Baptiste Saleil http://bsaleil.org/
// * Arnaud Bonatti https://github.com/Obsidien
//
// Licence: GPLv2+

const ExtensionUtils = imports.misc.extensionUtils;
const Extension = ExtensionUtils.getCurrentExtension();
const TodoTxtManager = Extension.imports.libs.todoTxtManager;

/* exported init */
function init(ignored_metadata) {
    ExtensionUtils.initTranslations();
    return new TodoTxtManager.TodoTxtManager();
}

/* vi: set expandtab tabstop=4 shiftwidth=4: */
