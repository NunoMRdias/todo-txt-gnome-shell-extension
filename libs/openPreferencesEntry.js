const GObject = imports.gi.GObject;
const PopupMenu = imports.ui.popupMenu;
const Gettext = imports.gettext;
const _ = Gettext.domain('todotxt').gettext;
const ExtensionUtils = imports.misc.extensionUtils;

/* exported OpenPreferencesEntry */
var OpenPreferencesEntry = GObject.registerClass({
    GTypeName: 'OpenPreferencesEntry'
}, class extends PopupMenu.PopupImageMenuItem {
    _init(data, params) {
        super._init(_("Open preferences"), 'settings-symbolic', params);
        this.connect('activate', (ignored_object, ignored_event) => {
            ExtensionUtils.openPrefs();
        });
    }
});

/* vi: set expandtab tabstop=4 shiftwidth=4: */
