const ExtensionUtils = imports.misc.extensionUtils;
const SettingsType = ExtensionUtils.getCurrentExtension().imports.preferences.types.settingsType.SettingsType;

/* exported IntegerType */
var IntegerType = class extends SettingsType {
    get_widget() {
        return 'combo';
    }

    get_signal() {
        return 'notify::selected';
    }

    get_value_from_widget(object) {
        return object.get_selected();
    }

    update_widget(widget, setting_value) {
        widget.set_selected(setting_value);
    }
};
