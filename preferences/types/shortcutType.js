const { Gtk, Gdk } = imports.gi;

const ExtensionUtils = imports.misc.extensionUtils;
const Extension = ExtensionUtils.getCurrentExtension();
const SettingsType = Extension.imports.preferences.types.settingsType.SettingsType;
const Utils = Extension.imports.libs.utils;

/* exported ShortcutType */
var ShortcutType = class extends SettingsType {
    get_widget() {
        return 'shortcut';
    }

    get_row() {
        return this.get_widget();
    }

    extra(params) {
        const accelerator_label = params.builder.get_object(`${params.setting}-shortcut-accelerator-label`);
        if (accelerator_label === null) {
            params.logger.error(`No accelerator_label for ${params.setting}`);
            return;
        }
        accelerator_label.set_text(params.settings.get(params.setting));
        params.widget.connect('activated', (ignored_object) => {
            this._selectShortcut(params.setting, params.settings, accelerator_label, params.builder, params.logger);
        });
    }

    additional_templates() {
        return [`${Extension.path}/preferences/ui/shortcutEditor.xml`];
    }

    _selectShortcut(shortcut, settings, label, builder, logger) {
        const dialog = builder.get_object('shortcut-editor-dialog');
        if (dialog === null) {
            logger.error(`No dialog for ${shortcut}`);
            return;
        }
        const controller = builder.get_object('shortcut-editor-event-controller');
        if (controller === null) {
            logger.error(`No controller for ${shortcut}`);
            return;
        }
        const accelerator_label = builder.get_object(`${shortcut}-shortcut-accelerator-label`);
        if (accelerator_label === null) {
            logger.error(`No accelerator_label for ${shortcut}`);
            return;
        }
        accelerator_label.set_text(settings.get(shortcut));
        controller.connect('key-pressed', (widget, keyval, keycode, state) => {
            let mask = state & Gtk.accelerator_get_default_mod_mask();
            mask &= ~Gdk.ModifierType.LOCK_MASK;

            if (mask === 0 && keyval === Gdk.KEY_Escape) { // eslint-disable-line no-magic-numbers
                logger.info(`${shortcut} not changed`);
                dialog.close();
                return Gdk.EVENT_STOP;
            }

            if (mask === 0 && keyval === Gdk.KEY_BackSpace) { // eslint-disable-line no-magic-numbers
                settings.set(shortcut, '');
                label.set_text('');
                dialog.close();
                return Gdk.EVENT_STOP;
            }

            if (
                !Utils.isBindingValid({ mask, keycode, keyval }) ||
                !Utils.isAccelValid({ mask, keyval })
            ) {
                return Gdk.EVENT_STOP;
            }


            const keybinding = Gtk.accelerator_name_with_keycode(
                null,
                keyval,
                keycode,
                mask
            );
            settings.set(shortcut, keybinding);
            label.set_text(keybinding);
            dialog.close();
            return Gdk.EVENT_STOP;
        });
        dialog.present();
    }
};

